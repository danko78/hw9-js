const tabNavs = document.querySelectorAll('.tabs-title');
const tabPanes = document.querySelectorAll('.tab-pane');

for (i = 0; i < tabNavs.length; i++) {
    tabNavs[i].addEventListener("click", function(e) {
        // e.preventDefault();
        const activeTabAttr = e.target.getAttribute('data-tab');

        for (x = 0; x < tabNavs.length; x++) {
            const contentAttr = tabPanes[x].getAttribute('data-tab-content');

            if (activeTabAttr === contentAttr) {
                tabNavs[x].classList.add('active');
                tabPanes[x].classList.add('active');
            } else {
                tabNavs[x].classList.remove('active');
                tabPanes[x].classList.remove('active');
            };
        };
    });
};